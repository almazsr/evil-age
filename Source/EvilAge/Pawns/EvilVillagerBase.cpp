#include "EvilVillagerBase.h"
#include "GameMode/EvilGameModeBase.h"

// Sets default values
AEvilVillagerBase::AEvilVillagerBase()
{
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEvilVillagerBase::BeginPlay()
{
	Super::BeginPlay();
	
	// Bind events
	AEvilGameModeBase* GameMode = (AEvilGameModeBase*)GetWorld()->GetAuthGameMode();
	GameMode->OnTimeStateChanged.AddUObject(this, &AEvilVillagerBase::OnTimeStateChanged);
}

// Called every frame
void AEvilVillagerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEvilVillagerBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

