// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GeneticAlgorithm/EvilGenome.h"
#include "GeneticAlgorithm/EvilPhenotype.h"
#include "GameMode/TimeState.h"
#include "EvilEnemyBase.generated.h"

UCLASS()
class EVILAGE_API AEvilEnemyBase : public ACharacter
{
	GENERATED_BODY()

public:
	AEvilEnemyBase();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (ExposeOnSpawn = "true"))
	FEvilGenome EvilGenome;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FEvilPhenotype EvilPhenotype;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Fitness")
	int32 NumberOfKills;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Fitness")
	float LifeSpan;

	UFUNCTION(BlueprintImplementableEvent)
	void OnTimeStateChanged(TimeState timeState);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
