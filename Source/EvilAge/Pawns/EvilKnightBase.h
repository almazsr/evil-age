// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameMode/TimeState.h"
#include "EvilKnightBase.generated.h"

UCLASS()
class EVILAGE_API AEvilKnightBase : public ACharacter
{
	GENERATED_BODY()

public:
	AEvilKnightBase();

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
	void OnTimeStateChanged(TimeState timeState);

public:
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
