#include "EvilKnightBase.h"
#include "GameMode/EvilGameModeBase.h"

// Sets default values
AEvilKnightBase::AEvilKnightBase()
{
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEvilKnightBase::BeginPlay()
{
	Super::BeginPlay();
	
	// Bind events
	AEvilGameModeBase* GameMode = (AEvilGameModeBase*)GetWorld()->GetAuthGameMode();
	GameMode->OnTimeStateChanged.AddUObject(this, &AEvilKnightBase::OnTimeStateChanged);
}

// Called every frame
void AEvilKnightBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEvilKnightBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

