// Fill out your copyright notice in the Description page of Project Settings.

#include "EvilEnemyBase.h"
#include "GameMode/EvilGameModeBase.h"
#include "Engine/Engine.h"
#include "GeneticAlgorithm/EvilGenerationLibrary.h"

AEvilEnemyBase::AEvilEnemyBase() : NumberOfKills(0)
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AEvilEnemyBase::BeginPlay()
{
	Super::BeginPlay();
	
	// Create characteristics
	EvilPhenotype = UEvilGenerationLibrary::EvilGenomeToEvilPhenotype(EvilGenome);

	// Bind events
	AEvilGameModeBase* GameMode = (AEvilGameModeBase*) GetWorld()->GetAuthGameMode();
	GameMode->OnTimeStateChanged.AddUObject(this, &AEvilEnemyBase::OnTimeStateChanged);
}

void AEvilEnemyBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
