#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameMode/TimeState.h"
#include "EvilVillagerBase.generated.h"

UCLASS()
class EVILAGE_API AEvilVillagerBase : public ACharacter
{
	GENERATED_BODY()

public:
	AEvilVillagerBase();

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
	void OnTimeStateChanged(TimeState timeState);

public:
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
