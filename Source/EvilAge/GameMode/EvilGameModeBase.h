// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TimeState.h"
#include "EvilGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class EVILAGE_API AEvilGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	AEvilGameModeBase(const FObjectInitializer& ObjectInitializer);

protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	void DestroyAllEnemies();

	void UpdateTimeState();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int NumberOfMonstersToSpawnPerAreas;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TimeStep;

public:
	DECLARE_EVENT_OneParam(AEvilGameModeBase, FTimeStateChanged, TimeState);
	DECLARE_EVENT_OneParam(AEvilGameModeBase, FSpawnEnemyWave, int);

	FTimeStateChanged OnTimeStateChanged;
	FSpawnEnemyWave OnSpawnEnemyWave;

	UFUNCTION(BlueprintCallable, meta = (ToolTip="Between 0 and 1. Day is between 0 and 0.5, Night between 0.5 and 1"))
	float GetTimeStateContinuous() { return TimeStateContinuous; }

private:
	float TimeStateContinuous; // between 0 and 1. At 0.5 it is midnight
	TimeState CurrentTimeState;
};
