// Fill out your copyright notice in the Description page of Project Settings.


#include "EvilPlayerControllerBase.h"

AEvilPlayerControllerBase::AEvilPlayerControllerBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	SetHidden(false);
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
}
