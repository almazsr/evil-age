// MIT License

#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class TimeState : uint8 {
	MORNING = 0 UMETA(DisplayName = "Morning"),
	NOON = 1 UMETA(DisplayName = "Noon"),
	AFTERNOON = 2 UMETA(DisplayName = "Afternoon"),
	EVENING = 3 UMETA(DisplayName = "Evening"),
	MIDNIGHT = 4 UMETA(DisplayName = "Midnight"),
	BEFORE_MORNING = 5 UMETA(DisplayName = "Before morning")
};
