// Copyright Epic Games, Inc. All Rights Reserved.


#include "EvilGameModeBase.h"
#include "EvilPlayerControllerBase.h"
#include "EvilGameStateBase.h"
#include "Pawns/EvilGodBase.h"
#include "UI/EvilWidgetBase.h"

AEvilGameModeBase::AEvilGameModeBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer), NumberOfMonstersToSpawnPerAreas(1), TimeStep(0.001f), 
	  TimeStateContinuous(0.f), CurrentTimeState(TimeState::MORNING)
{
	PrimaryActorTick.bCanEverTick = true;
	PlayerControllerClass = AEvilPlayerControllerBase::StaticClass();
	DefaultPawnClass = AEvilGodBase::StaticClass();
	GameStateClass = AEvilGameStateBase::StaticClass();
	HUDClass = UEvilWidgetBase::StaticClass();
}

void AEvilGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	OnTimeStateChanged.Broadcast(CurrentTimeState);
}

void AEvilGameModeBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	UpdateTimeState();
}

void AEvilGameModeBase::DestroyAllEnemies()
{
	
}

void AEvilGameModeBase::UpdateTimeState()
{
	// TODO handle directionnal light position
	TimeStateContinuous += TimeStep;
	switch (CurrentTimeState)
	{
	case TimeState::MORNING:
		if (TimeStateContinuous >= 0.17f)
		{
			CurrentTimeState = TimeState::NOON;
			OnTimeStateChanged.Broadcast(CurrentTimeState);
		}
		break;

	case TimeState::NOON:
		if (TimeStateContinuous >= 0.34f)
		{
			CurrentTimeState = TimeState::AFTERNOON;
			OnTimeStateChanged.Broadcast(CurrentTimeState);
		}
		break;

	case TimeState::AFTERNOON:
		if (TimeStateContinuous >= 0.5f)
		{
			CurrentTimeState = TimeState::EVENING;
			OnTimeStateChanged.Broadcast(CurrentTimeState);
			
		}
		break;

	case TimeState::EVENING:
		if (TimeStateContinuous >= 0.68f)
		{
			CurrentTimeState = TimeState::MIDNIGHT;
			OnTimeStateChanged.Broadcast(CurrentTimeState);
			OnSpawnEnemyWave.Broadcast(NumberOfMonstersToSpawnPerAreas);
			// TODO put lights on
		}
		break;

	case TimeState::MIDNIGHT:
		if (TimeStateContinuous >= 0.85f)
		{
			CurrentTimeState = TimeState::BEFORE_MORNING;
			OnTimeStateChanged.Broadcast(CurrentTimeState);
		}
		break;

	case TimeState::BEFORE_MORNING:
		if (TimeStateContinuous >= 1.f)
		{
			CurrentTimeState = TimeState::MORNING;
			OnTimeStateChanged.Broadcast(CurrentTimeState);
			DestroyAllEnemies();
			TimeStateContinuous = 0.f;
			// TODO lights off 
		}
		break;

	default: break;
	}
}
