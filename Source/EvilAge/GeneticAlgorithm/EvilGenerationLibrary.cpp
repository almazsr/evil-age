// Fill out your copyright notice in the Description page of Project Settings.


#include "EvilGenerationLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "Pawns/EvilEnemyBase.h"

FEvilGenome UEvilGenerationLibrary::CreateRandomEnemyEvilGenome(const uint8 pointsToDistribute)
{
	FEvilGenome EvilGenome;
	return EvilGenome;
}

FEvilPhenotype UEvilGenerationLibrary::EvilGenomeToEvilPhenotype(const FEvilGenome& EvilGenome)
{
	FEvilPhenotype EvilPhenotype;
	EvilPhenotype.HalfSight = FMath::Clamp(EvilGenome.Vision * 3, 0, 180);
	return EvilPhenotype;
}

TArray<AEvilEnemyBase*> UEvilGenerationLibrary::SortEnemiesByFitness(TArray<AEvilEnemyBase*> enemies)
{
	enemies.Sort([](const AEvilEnemyBase& a, const AEvilEnemyBase& b)
	{
		// TODO
		return false;
	});
	return enemies;
}

TArray<FEvilGenome> UEvilGenerationLibrary::MutateEvilGenomes(TArray<AEvilEnemyBase*> monsters, uint8 count)
{
	return TArray<FEvilGenome>();
}
