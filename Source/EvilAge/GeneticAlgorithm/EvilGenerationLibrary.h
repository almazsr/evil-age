// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EvilGenome.h"
#include "EvilPhenotype.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "EvilGenerationLibrary.generated.h"

class AEvilEnemyBase; // Forward declaration

/**
 * 
 */
UCLASS()
class EVILAGE_API UEvilGenerationLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "Genetic Algorithm")
	static FEvilGenome CreateRandomEnemyEvilGenome(const uint8 pointsToDistribute);

	UFUNCTION(BlueprintCallable, Category = "Genetic Algorithm")
	static FEvilPhenotype EvilGenomeToEvilPhenotype(const FEvilGenome& EvilGenome);

	UFUNCTION(BlueprintCallable, Category = "Genetic Algorithm")
	static TArray<AEvilEnemyBase*> SortEnemiesByFitness(TArray<AEvilEnemyBase*> enemies);

	UFUNCTION(BlueprintCallable, Category = "Genetic Algorithm")
	static TArray<FEvilGenome> MutateEvilGenomes(TArray<AEvilEnemyBase*> enemies, uint8 count);
};
