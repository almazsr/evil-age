// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EvilGenome.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct EVILAGE_API FEvilGenome
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 Speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 Force;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 Vision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 Health;

	FEvilGenome()
	{
		Speed = 0;
		Force = 0;
		Vision = 0;
		Health = 0;
	}
};
