// MIT License


#include "Area/EvilEnemySpawnArea.h"
#include "GameMode/EvilGameModeBase.h"

AEvilEnemySpawnArea::AEvilEnemySpawnArea()
{

}

// Called when the game starts or when spawned
void AEvilEnemySpawnArea::BeginPlay()
{
	Super::BeginPlay();

	// Bind events
	AEvilGameModeBase* GameMode = (AEvilGameModeBase*)GetWorld()->GetAuthGameMode();
	GameMode->OnSpawnEnemyWave.AddUObject(this, &AEvilEnemySpawnArea::OnSpawnNewEnemyWave);
}
