// MIT License

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EvilConstructibleAreaBase.generated.h"

UCLASS()
class EVILAGE_API AEvilConstructibleAreaBase : public AActor
{
	GENERATED_BODY()
	
public:	
	AEvilConstructibleAreaBase();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

};
