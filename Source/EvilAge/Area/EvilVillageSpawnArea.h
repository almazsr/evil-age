// MIT License

#pragma once

#include "CoreMinimal.h"
#include "Area/EvilSpawnAreaBase.h"
#include "EvilVillageSpawnArea.generated.h"

/**
 * 
 */
UCLASS()
class EVILAGE_API AEvilVillageSpawnArea : public AEvilSpawnAreaBase
{
	GENERATED_BODY()
	
};
