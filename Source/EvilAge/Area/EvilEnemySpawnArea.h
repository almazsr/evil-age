// MIT License

#pragma once

#include "CoreMinimal.h"
#include "Area/EvilSpawnAreaBase.h"
#include "EvilEnemySpawnArea.generated.h"

/**
 * 
 */
UCLASS()
class EVILAGE_API AEvilEnemySpawnArea : public AEvilSpawnAreaBase
{
	GENERATED_BODY()
	
public:
	AEvilEnemySpawnArea();

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
	void OnSpawnNewEnemyWave(int numberToSpawn);
};
