// MIT License


#include "Building/EvilBuildingWithEntry.h"
#include "Components/BoxComponent.h"
#include "GameMode/EvilGameModeBase.h"

AEvilBuildingWithEntry::AEvilBuildingWithEntry()
{
	EntryCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	EntryCollider->SetupAttachment(Mesh);

	EntryCollider->SetWorldScale3D(FVector(10.f, 10.f, 2.f));
	FVector newLocation = GetActorLocation();
	newLocation.X += 400.f;
	EntryCollider->SetWorldLocation(newLocation);
}

void AEvilBuildingWithEntry::BeginPlay()
{
	Super::BeginPlay();

	// Bind events
	AEvilGameModeBase* GameMode = (AEvilGameModeBase*)GetWorld()->GetAuthGameMode();
	GameMode->OnTimeStateChanged.AddUObject(this, &AEvilBuildingWithEntry::OnTimeStateChanged);
}
