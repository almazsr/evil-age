// MIT License

#pragma once

#include "CoreMinimal.h"
#include "Building/EvilBuildingBase.h"
#include "EvilBuildingWithEntry.generated.h"

/**
 * 
 */
UCLASS()
class EVILAGE_API AEvilBuildingWithEntry : public AEvilBuildingBase
{
	GENERATED_BODY()
	
public:
	AEvilBuildingWithEntry();

	// TODO callback to get entry position for villagers ?

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* EntryCollider;
};
