// MIT License

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameMode/TimeState.h"
#include "EvilBuildingBase.generated.h"

UCLASS()
class EVILAGE_API AEvilBuildingBase : public AActor
{
	GENERATED_BODY()
	
public:
	AEvilBuildingBase();

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
	void OnTimeStateChanged(TimeState timeState);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UStaticMeshComponent* Mesh;

public:
	virtual void Tick(float DeltaTime) override;
};
