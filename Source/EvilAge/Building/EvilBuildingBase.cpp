// MIT License

#include "Building/EvilBuildingBase.h"
#include "GameMode/EvilGameModeBase.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AEvilBuildingBase::AEvilBuildingBase()
{
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetWorldScale3D(FVector(0.5f));
	RootComponent = Mesh;
}

// Called when the game starts or when spawned
void AEvilBuildingBase::BeginPlay()
{
	Super::BeginPlay();
	
	// Bind events
	AEvilGameModeBase* GameMode = (AEvilGameModeBase*)GetWorld()->GetAuthGameMode();
	GameMode->OnTimeStateChanged.AddUObject(this, &AEvilBuildingBase::OnTimeStateChanged);
}

// Called every frame
void AEvilBuildingBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

