// MIT License

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameMode/TimeState.h"
#include "EvilWidgetBase.generated.h"

/**
 * 
 */
UCLASS()
class EVILAGE_API UEvilWidgetBase : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	virtual void NativeOnInitialized() override;

	UFUNCTION(BlueprintImplementableEvent)
	void OnTimeStateChanged(TimeState timeState);
};
