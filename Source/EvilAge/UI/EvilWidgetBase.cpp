// MIT License


#include "UI/EvilWidgetBase.h"
#include "GameMode/EvilGameModeBase.h"

// Called when the game starts or when spawned
void UEvilWidgetBase::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	// Bind events
	AEvilGameModeBase* GameMode = (AEvilGameModeBase*)GetWorld()->GetAuthGameMode();
	GameMode->OnTimeStateChanged.AddUObject(this, &UEvilWidgetBase::OnTimeStateChanged);
}
